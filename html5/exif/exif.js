/*
 * Drag and drop functionality
 */
function dragEnter(e) {
	e.stopPropagation();
	e.preventDefault();
	dropbox.style.border = '10px solid green';
}

function dragExit(e) {
	e.stopPropagation();
	e.preventDefault();
	dropbox.style.border = '10px solid white';
}

function dragDrop(e) {
	dragExit(e);
	var files = e.dataTransfer.files;
	if (files.length > 0) {
		submitFiles(files);
	}
}
function clickUpload(e) {
	e.stopPropagation();
	e.preventDefault();
	if (fileInput) {
		fileInput.click();
	}
}

function setupDropbox() {
	var dropbox = document.getElementById("dropbox");
	var fileInput = document.getElementById("fileInput");
	var results = document.getElementById("results");
	dropbox.addEventListener("dragenter", dragEnter, false);
	dropbox.addEventListener("dragover", dragEnter, false);
	dropbox.addEventListener("dragexit", dragExit, false);
	dropbox.addEventListener("dragleave", dragExit, false);
	dropbox.addEventListener("drop", dragDrop, false);
	dropbox.addEventListener("click", clickUpload, false);
}

/*
 * File Submission
 */
function submitFiles(files) {
	for (var i = 0; i < files.length; ++i) {
		var file = files[i];
		var formData = new FormData();

		var name = "image" + i;
		if (file.type == "image/jpeg") {
			formData.append(name, file);
			sendRequest(formData);
		}
	}
}

function sendRequest(data) {
	var req = new XMLHttpRequest();
	req.addEventListener("load", transferComplete, false);
	req.addEventListener("error", transferFailed, false);
	req.open("POST", "");
	req.send(data);
}

function transferComplete(e) {
	console.log("complete");
	var newDiv = document.createElement("div");
	newDiv.classList.add("res");
	newDiv.innerHTML = "TODO append results from AJAX call";
	results.appendChild(newDiv);
}

function transferFailed(e) {
	console.log("failed");
}

window.onload = setupDropbox;